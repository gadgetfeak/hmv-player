This app plays randoms rounds of FapHero Video. (Soon to support other HMV)

# Instructions


## Requirements

make sure you have [Visual C++ Redistributable 2015 x86](https://www.microsoft.com/en-us/download/details.aspx?id=48145) installed already

## Usage

Extract all the files to folder with faphero videos or other videos.  
Launch the HMV Player  
Press the play button

# Modes 

*Currently only FH mode is supported.*

you can select either Simply Random or Hero

For Simply Random if you want random rounds playing. You can select "include Final round" if you want the last rounds to be mixed in.

For hero, enter your Level in the text box. Higher the level the more rounds you play with the last video play being a FH final round
Currently the CHDB.xml does not have each rounds the appropriate skill Rating (should be between 0.1 to 1). So currently all rounds have 1, by selecting Hero mode with lvl 3, you will play only 3 rounds.

Select "Use Round Break Time" for using the videos break time before the next round
If unselected, You can enter the Min break time (Seconds) and max break time in the two text box below. If you dont want any break time, keep both values as 0.

## Special Feature
**Go to Cum Count Down**  
If you want to end soon, press the button C (on Keyboard) or click CD button to go to cum Count Down of the current video

**Go to Cum Moment**  
If you want to finish immediately, press the the button M (on Keyboard) or click CM to go to next cum moment of the current round.

**Note**  
Currently in the CHDB.xml, only Cock Hero - Paradise (by Bastati) has the cum count down and cum comment Data. (It would help me a lot if some filled for the videos)

## Controls

Enter -> Fullscreen Toggle  
Space -> Fapue  
Left Arrow -> Next Round  
C -> Goto Count Down  
M -> Goto Cum Moment  

# TroubleShooting

If a video doesnt play, its probably because the video names dont match with the names on CHDB.xml or the CHDB.xml is missing or has bad formatting.

If a video doesnt load and you dont get an error message. One of the video link may be wrong.

# Making your own playlist

## Adding a new Video to the FH playlist

Here are instructions to modify the CHDB.xml

After the \<CH\> you can start making your 
new entry. A new video starts with \<VIDEO\> and ends with \</VIDEO\>. 
**Do note the system is case senistive**

Here is an Example with how to fill the contents for a video.

\<?xml version="1.0" encoding="utf-8" ?\>  
<CH\>  
---<VIDEO\>  
------   <Filename\>Cock Hero - Hentai Edition.avi</Filename\>   <---------- File name should match withe video name  
------    <Round\>  ------ <----------- Add a round  
 ---------     <StartTime\>00:00:05</StartTime\>   <----------- Define the start of the round. Time in hh:mm:ss  
---------      <EndTime\>00:04:09</EndTime\>   <----------- Define the end of the round.  
 ---------     <SkillRaiting\>1</SkillRaiting\>   <----------- Define the defficulty of the round. Supposedly between 0.1 to 1  
 ------   </Round\>  
 ------   <Round\>  
---------       <StartTime\>00:04:40</StartTime\>  
 ---------      <EndTime\>00:08:44</EndTime\>     <----------- Define the cum Count down for the video (Optional)  
 ---------      <SkillRaiting\>1</SkillRaiting\>  
------    </Round\>  
 --- </VIDEO\>  
---<VIDEO\>  
------    <Filename\>Cock Hero - Paradise (HD 1080p).wmv</Filename\>  \<----------- Still mandatory  even if you dont have the file  
------    <Link\>http://faphub.org/videos/Paradise.mp4</Link\>   \<----------- Link to the video (Optional, Use it if you dont have File) 
------    <EndCumTime\>00:45:17</EndCumTime\>  
------    <Round\>  
 ---------      <StartTime\>00:03:08</StartTime\>  
---------       <EndTime\>00:12:27</EndTime\>  
 ---------      <SkillRaiting\>1</SkillRaiting\>  
---------       <CumMoment\>00:05:19</CumMoment>     <----------- Define the cum moment for round (Optional)  
 ------   </Round\>  
------    <Round\>  
 ---------      <StartTime\>00:26:46</StartTime\>  
 ---------      <EndTime\>00:30:52</EndTime\>  
 ---------      <SkillRaiting\>1</SkillRaiting\>  
---------       <CumMoment\>00:28:24</CumMoment\>  
 ---------      <CumMoment\>00:29:32</CumMoment\>     <----------- You can add multiple Cum Moment (Optional)  
 ------   </Round\>  
 --- </VIDEO\>   
.......  
</CH\>  

# Plans

Here are the list of the features and improvements I may be adding. If wish to request a feature, you can either create a new Issue or Message me over Discord.

## HMV Mode (Next in Line)
This mode mode can play videos from a folder or rounds from the FH playlist.  
You can limit how long a Video/Round will play.  
You can define a break time.  

## Game Mode
To play FH in a game like style. Similar to FH mode but may not be random and with Additional Features. TBD

## Tag System
You can define tags for each rounds or videos in the FH playlist. When playing in HMV mode (if loaded by playlist) or FH mode, you can filter the videos by tags.

## Playlist Editor  **(Work in Progress)**
In this editor, you play through the video and define the start of a round, the end, Cum Moments and a the Cum Count Down.

## Improvements
* Load FH mode playlist.  
* Load multiple playlist.
* Error Handling if stream fails
* A slider for controlling the video
* A way to recognize FapHero with different file Names / Format
* Displaying the list of videos
* Display a message box if no matching video is found (Next in Line)
* Display a message box if the playlist format is incorrect
* A display showing the name of the video
* A display showing the Round number (of the video) and the time remaining
* Better GUI - Mostlikely dark themed
* Improve random play from playing a played video

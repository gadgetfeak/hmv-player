// HMV Player.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "HMV Player.h"
#include "VideoPlayer.h"
#include "PlayListEditor.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR szVidChild[MAX_LOADSTRING];


#pragma region Global Variables

HWND vidHwnd;
HMENU mainHmenu;
RECT mainRect;

HWND playButton, restartButton, fullScreenButton, muteButton, NextButton;
HMENU playMenu = (HMENU)IDM_PlayMENU, restartMenu = (HMENU)IDM_RestartMENU, fullScreenMenu = (HMENU)IDM_FullScreenMENU, muteMenu = (HMENU)IDM_MuteMENU, NextMenu = (HMENU)IDM_NextMENU;
HWND cumCountDownButton, NextCumMomentButton, FHButton, HMVButton;
HMENU CumCountDownMenu = (HMENU)IDM_CumCountDownMENU, NextCumMomentMenu = (HMENU)IDM_NextCumMomentMENU, FHMenu = (HMENU)IDM_FHMENU, HMVMenu = (HMENU)IDM_HMVMENU;
HWND simplyRandomButton, includeFinalRoundButton, skillBasedButton, skillLevelTxt, useVideoBreakTimeButton, minBreakTimeBox, maxBreakTimeBox;
HMENU simplyRandomMenu = (HMENU)IDM_SimplyRandomMENU, includeFinalRoundMenu = (HMENU)IDM_IncludeFinalRoundMENU, skillBasedMenu = (HMENU)IDM_SkillBasedMENU, skillLevelMenu = (HMENU)IDM_SkillLevelMENU, 
	useVideoBreakTimeMenu = (HMENU)IDM_UseVideoBreakTimeMENU, minBreakTimeMenu = (HMENU)IDM_MinBreakTimeMENU, maxBreakTimeMenu = (HMENU)IDM_MaxBreakTimeMENU;

FHConfig fhConfig;
#pragma endregion


// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	VidWndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void FullScreenSwitch(HWND);
void showFHhwnd(HWND, bool);
int wcharToInt(wchar_t* sqrt_buffer);
void getChoices();

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_HMVPLAYER, szWindowClass, MAX_LOADSTRING);
	LoadStringW(hInstance, IDS_VID_TITLE, szVidChild, MAX_LOADSTRING);
    MyRegisterClass(hInstance);
    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_HMVPLAYER));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

#pragma region Registering Class

//	MAIN CLASS
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_HMVPLAYER));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_HMVPLAYER);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}


#pragma endregion

#pragma region Window Initialization

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
      CW_USEDEFAULT, 0, 1280, 720, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}


#pragma endregion

#pragma region Windows Callbacks

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

wchar_t* txt_buffer = new wchar_t[2 + 1];
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//OutputDebugString(_T("" + message));
    switch (message)
    {
    case WM_COMMAND:
        {
		int wmId = LOWORD(wParam);
		if (wmId >= 500 && wmId < 540)
		{
			SetFocus(hWnd);
			switch (wmId) {
			case IDM_PlayMENU: //play
				if(isVideoLoaded())
					playerControl(vidHwnd, VK_SPACE);
				else 
				{
					getChoices();
					setFhMode(vidHwnd, fhConfig);
				}
				break;
			case IDM_NextMENU:
				//<TODO> for the keys in videoplayer.cpp
				nextVideo(vidHwnd);
				break;
			case IDM_RestartMENU: //Restart
				getChoices();
				setFhMode(vidHwnd, fhConfig);
				break;
			case IDM_CumCountDownMENU:
				playerControl(vidHwnd, 'C');
				break;
			case IDM_NextCumMomentMENU:
				playerControl(vidHwnd, 'M');
				break;
			case IDM_FullScreenMENU: //FullScreen
				FullScreenSwitch(hWnd);
				break;
			case IDM_MuteMENU: //Mute
				playerControl(vidHwnd, wParam);
				break;
#pragma region FH Menu

			case IDM_FHMENU:
				if (SendMessage(FHButton, BM_GETCHECK, 0, 0))
				{
					SendMessage(HMVButton, BM_SETCHECK, BST_UNCHECKED, 0);
					showFHhwnd(hWnd, true);
				}
				else
					SendMessage(FHButton, BM_SETCHECK, BST_CHECKED, 0);
				break;

			case IDM_SimplyRandomMENU:
				if (SendMessage(simplyRandomButton, BM_GETCHECK, 0, 0))
				{
					SendMessage(skillBasedButton, BM_SETCHECK, BST_UNCHECKED, 0);
					ShowWindow(includeFinalRoundButton, SW_SHOW);
					ShowWindow(skillLevelTxt, SW_HIDE);
				}
				else
					SendMessage(simplyRandomButton, BM_SETCHECK, BST_CHECKED, 0);
				break;

			case IDM_SkillBasedMENU:
				if (SendMessage(skillBasedButton, BM_GETCHECK, 0, 0))
				{
					SendMessage(simplyRandomButton, BM_SETCHECK, BST_UNCHECKED, 0);
					ShowWindow(includeFinalRoundButton, SW_HIDE);
					ShowWindow(skillLevelTxt, SW_SHOW);
				}
				else
					SendMessage(skillBasedButton, BM_SETCHECK, BST_CHECKED, 0);
				break;

#pragma endregion
			case IDM_HMVMENU:
				if (SendMessage(HMVButton, BM_GETCHECK, 0, 0))
				{
					SendMessage(FHButton, BM_SETCHECK, BST_UNCHECKED, 0);
					showFHhwnd(hWnd, false);
				}
				else
					SendMessage(HMVButton, BM_SETCHECK, BST_CHECKED, 0);
				break;
			}
			break;
		}
		else
		{
			// Parse the menu selections:
			switch (wmId)
			{
			case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			case IDM_PlayEditor:
				initPLEditor(hInst);
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
			FillRect(hdc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW));
            EndPaint(hWnd, &ps);
        }
        break;
	case WM_CREATE:
		CLIENTCREATESTRUCT ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 1);
		mainHmenu = GetMenu(hWnd);
		ccs.idFirstChild = ID_MDI_FIRSTCHILD;
		vidHwnd = CreateWindowEx(0, TEXT("MDICLIENT"), (LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN | CS_DBLCLKS, 0, 0, 896, 504, hWnd, NULL, hInst, (LPSTR)&ccs);
		ShowWindow(vidHwnd, SW_SHOW);
		//player(vidHwnd);

		//GUI
		//WS_TABSTOP
		int x;
		playButton = CreateWindow(L"BUTTON", L"► ", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x = 6, 510, 30, 25, hWnd, playMenu, hInst, NULL); x += 30;
		NextButton = CreateWindow(L"BUTTON", L"►►  ", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x += 6, 510, 30, 25, hWnd, NextMenu, hInst, NULL); x += 30;
		restartButton = CreateWindow(L"BUTTON", L"↺ ",  WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x += 6, 510, 30, 25, hWnd, restartMenu, hInst, NULL); x += 30;
		x += 30;
		cumCountDownButton = CreateWindow(L"BUTTON", L"CD", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x += 6, 510, 30, 25, hWnd, CumCountDownMenu, hInst, NULL); x += 30;
		NextCumMomentButton = CreateWindow(L"BUTTON", L"CM", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x += 6, 510, 30, 25, hWnd, NextCumMomentMenu, hInst, NULL); x += 30;
		x += 30;
		fullScreenButton = CreateWindow(L"BUTTON", L"☐", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x += 6, 510, 30, 25, hWnd, fullScreenMenu, hInst, NULL); x += 30;
		muteButton = CreateWindow(L"BUTTON", L"Mute",  WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX | BS_CHECKBOX, x += 6, 510, 52, 25, hWnd, muteMenu, hInst, NULL); x += 52;
		x += 30;
		FHButton = CreateWindow(L"BUTTON", L"FH", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX | BS_CHECKBOX, x += 6, 510, 45, 25, hWnd, FHMenu, hInst, NULL); x += 45;
		HMVButton = CreateWindow(L"BUTTON", L"HMV", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX | BS_CHECKBOX, x += 6, 510, 52, 25, hWnd, HMVMenu, hInst, NULL); x += 50;


		simplyRandomButton = CreateWindow(L"BUTTON", L"Simply Random", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX | BS_CHECKBOX, 6, 541, 125, 25, hWnd, simplyRandomMenu, hInst, NULL);
		includeFinalRoundButton = CreateWindow(L"BUTTON", L"Include Finale Round", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX | BS_CHECKBOX, 137, 541, 160, 25, hWnd, includeFinalRoundMenu, hInst, NULL);
		skillBasedButton = CreateWindow(L"BUTTON", L"Hero Mode", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX | BS_CHECKBOX, 6, 572, 95, 25, hWnd, skillBasedMenu, hInst, NULL);
		skillLevelTxt = CreateWindow(L"EDIT", L"3", WS_VISIBLE | WS_CHILD | ES_NUMBER | ES_CENTER, 107, 572, 52, 25, hWnd, skillLevelMenu, hInst, NULL);
		useVideoBreakTimeButton = CreateWindow(L"BUTTON", L"Use Round Break Time", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX | BS_CHECKBOX, 6, 603, 170, 25, hWnd, useVideoBreakTimeMenu, hInst, NULL);

		minBreakTimeBox = CreateWindow(L"EDIT", L"30", WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 6, 634, 52, 25, hWnd, minBreakTimeMenu, hInst, NULL);
		maxBreakTimeBox = CreateWindow(L"EDIT", L"45", WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 64, 634, 52, 25, hWnd, maxBreakTimeMenu, hInst, NULL);

		//defaults
		SendMessage(skillLevelTxt, EM_SETLIMITTEXT, 2, 0);
		SendMessage(minBreakTimeBox, EM_SETLIMITTEXT, 2, 0);
		SendMessage(maxBreakTimeBox, EM_SETLIMITTEXT, 2, 0);

		SendMessage(FHButton, BM_SETCHECK, BST_CHECKED, 0);
		SendMessage(simplyRandomButton, BM_SETCHECK, BST_CHECKED, 0);
		SendMessage(useVideoBreakTimeButton, BM_SETCHECK, BST_CHECKED, 0);
		ShowWindow(includeFinalRoundButton, SW_SHOW);
		ShowWindow(skillLevelTxt, SW_HIDE);
		break;
	case WM_SIZE:
		if (wParam == SIZE_MAXIMIZED || wParam == SIZE_RESTORED) {
			if (GetWindowRect(hWnd, &mainRect))
			{
				int width = (mainRect.right - mainRect.left) * 0.7;
				int height = (width * 9) / 16;
				MoveWindow(vidHwnd, 0, 0, width, height, false);

				//Adjust GUI
				int x;
				MoveWindow(playButton, x = 6, height + 6, 30, 25, true); x += 30;
				MoveWindow(NextButton, x += 6, height + 6, 30, 25, true); x += 30;
				MoveWindow(restartButton, x += 6, height + 6, 30, 25, true); x += 30;
				x += 30;
				MoveWindow(cumCountDownButton, x += 6, height + 6, 30, 25, true); x += 30;
				MoveWindow(NextCumMomentButton, x += 6, height + 6, 30, 25, true); x += 30;
				x += 30;
				MoveWindow(fullScreenButton, x += 6, height + 6, 30, 25, true); x += 30;
				MoveWindow(muteButton, x += 6, height + 6, 52, 25, true); x += 52;
				x += 30;
				MoveWindow(FHButton, x += 6, height + 6, 45, 25, true); x += 45;
				MoveWindow(HMVButton, x += 6, height + 6, 52, 25, true); x += 50;

				MoveWindow(simplyRandomButton, 6, height + 37, 125, 25, true);
				MoveWindow(includeFinalRoundButton, 137, height + 37, 160, 25, true);
				MoveWindow(skillBasedButton, 6, height + 68, 95, 25, true);
				MoveWindow(skillLevelTxt, 107, height + 68, 52, 25, true);
				MoveWindow(useVideoBreakTimeButton, 6, height + 99, 170, 25, true);
				MoveWindow(minBreakTimeBox, 6, height + 130, 52, 25, true);
				MoveWindow(maxBreakTimeBox, 64, height + 130, 52, 25, true);
			}
		}
		break;
	case WM_KEYDOWN:
		//<TODO> specific keys

		if(!playerControl(vidHwnd, wParam))
			switch (wParam)
			{
			case VK_ESCAPE:
			case VK_RETURN:
				FullScreenSwitch(hWnd);
			}
		break;
	case WM_LBUTTONDBLCLK:
		FullScreenSwitch(hWnd);
//#if _DEBUG
//		TestFunction();
//#endif
		break;
    case WM_DESTROY:
		destroyPlayer();
        PostQuitMessage(0);
        break;
	default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

//LRESULT CALLBACK VidWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
//{
//	switch (message)
//	{
//	case WM_DESTROY:
//		PostQuitMessage(0);
//		break;
//	default:
//		return DefWindowProc(hWnd, message, wParam, lParam);
//	}
//	return 0;
//}


#pragma endregion

#pragma region GUI Function

void showFHhwnd(HWND hwnd, bool show) {
	ShowWindow(simplyRandomButton, SW_SHOW * show);
	ShowWindow(includeFinalRoundButton, SW_SHOW * show);
	ShowWindow(skillBasedButton, SW_SHOW * show);
	ShowWindow(skillLevelTxt, SW_SHOW * show);
	ShowWindow(useVideoBreakTimeButton, SW_SHOW * show);
}

int wcharToInt(wchar_t* sqrt_buffer) {
	int value = 0;
	for (int i = 0; sqrt_buffer[i] != '\0'; i++) {
		value = value * 10 + (sqrt_buffer[i] - '0');
	}
	return value;
}

void getChoices() {
	fhConfig.simplyRandom = SendMessage(simplyRandomButton, BM_GETCHECK, 0, 0);
	fhConfig.includeFinaleRound = SendMessage(includeFinalRoundButton, BM_GETCHECK, 0, 0);
	fhConfig.skillBased = SendMessage(skillBasedButton, BM_GETCHECK, 0, 0);
	GetWindowText(skillLevelTxt, txt_buffer, 3);
	fhConfig.skillLevel = wcharToInt(txt_buffer);
	fhConfig.useVideoBreakTIme = SendMessage(useVideoBreakTimeButton, BM_GETCHECK, 0, 0);
	GetWindowText(minBreakTimeBox, txt_buffer, 3);
	fhConfig.minBreakTime = wcharToInt(txt_buffer);
	GetWindowText(maxBreakTimeBox, txt_buffer, 3);
	fhConfig.maxBreakTime = wcharToInt(txt_buffer);
}

#pragma endregion


#pragma region FullScreen

BOOL IsWindowMode = TRUE;
WINDOWPLACEMENT wpc;
LONG HWNDStyle = 0;
LONG HWNDStyleEx = 0;

void FullScreenSwitch(HWND hwnd)
{
	if (IsWindowMode)
	{
		SetMenu(hwnd, NULL);
		IsWindowMode = FALSE;
		GetWindowPlacement(hwnd, &wpc);
		if (HWNDStyle == 0)
			HWNDStyle = GetWindowLong(hwnd, GWL_STYLE);
		if (HWNDStyleEx == 0)
			HWNDStyleEx = GetWindowLong(hwnd, GWL_EXSTYLE);

		LONG NewHWNDStyle = HWNDStyle;
		NewHWNDStyle &= ~WS_BORDER;
		NewHWNDStyle &= ~WS_DLGFRAME;
		NewHWNDStyle &= ~WS_THICKFRAME;

		LONG NewHWNDStyleEx = HWNDStyleEx;
		NewHWNDStyleEx &= ~WS_EX_WINDOWEDGE;

		SetWindowLong(hwnd, GWL_STYLE, NewHWNDStyle | WS_POPUP);
		SetWindowLong(hwnd, GWL_EXSTYLE, NewHWNDStyleEx | WS_EX_TOPMOST);
		ShowWindow(hwnd, SW_SHOWMAXIMIZED);
		if (GetWindowRect(hwnd, &mainRect))
		{
			int width = mainRect.right - mainRect.left;
			int height = mainRect.bottom - mainRect.top;
			MoveWindow(vidHwnd, 0, 0, width, height, false);
		}
	}
	else
	{
		SetMenu(hwnd, mainHmenu);
		IsWindowMode = TRUE;
		SetWindowLong(hwnd, GWL_STYLE, HWNDStyle);
		SetWindowLong(hwnd, GWL_EXSTYLE, HWNDStyleEx);
		ShowWindow(hwnd, SW_SHOWNORMAL);
		if (GetWindowRect(hwnd, &mainRect))
		{
			int width = (mainRect.right - mainRect.left) * 0.7;
			int height = (width * 9) / 16;
			MoveWindow(vidHwnd, 0, 0, width, height, false);
		}
		SetWindowPlacement(hwnd, &wpc);
	}
}

#pragma endregion

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#define IDM_PlayMENU 500
#define IDM_RestartMENU 501
#define IDM_FullScreenMENU 502
#define IDM_MuteMENU 503
#define IDM_NextMENU 504
#define IDM_CumCountDownMENU 505
#define IDM_NextCumMomentMENU 506

#define IDM_FHMENU 507
#define IDM_HMVMENU 508
#define IDM_PMVMENU 509

#define IDM_SimplyRandomMENU 510
#define IDM_IncludeFinalRoundMENU 511
#define IDM_SkillBasedMENU 512
#define IDM_SkillLevelMENU 513
#define IDM_UseVideoBreakTimeMENU 514
#define IDM_MinBreakTimeMENU 515
#define IDM_MaxBreakTimeMENU 516


//PlayList Window
#define IDM_PELoadPlaylist 600
#define IDM_PENewPlayList 601
#define IDM_AddVideo 602
#define IDM_EditVideo 603
#define IDM_PERoundInfo 604
#define IDM_RoundPoint 605
#define IDM_AddCumMoment 606
#define IDM_AddCumCountDown 607
#define IDM_UndoPEAction 608
#define IDM_PEdelete 609

//Menu 590+


// TODO: reference additional headers your program requires here

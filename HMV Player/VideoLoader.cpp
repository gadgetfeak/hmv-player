#include "stdafx.h"
#include "tinyxml2.h"
#include "vlcpp/vlc.hpp"
#include <filesystem>
#include <vector>
#include "VideoLoader.h"
#include <math.h>


using namespace tinyxml2;
using namespace std;
using namespace std::experimental;



//constants
const char* videoFileList = "CHDB.xml";

//Initialization
libvlc_time_t TimeTo_ms(const char* strTime);
float charToFloat(const char* strFloat);

//Global Variable
vector<videoInfo> videoList;

vector<videoInfo> LoadVideoList() {
	XMLDocument doc;
	videoList.clear();
	if (!filesystem::exists(videoFileList)) {
		//<TODO> display playlist name
		MessageBox(NULL, L"Playlist not found", NULL, MB_OK);
		return videoList;
	}
	//<TODO> add error handling for format
	doc.LoadFile(videoFileList);

	for (XMLNode* videoNode = doc.FirstChildElement("CH")->FirstChild(); videoNode; videoNode = videoNode->NextSibling())
	{
		videoInfo vPlaceHolder;
		vPlaceHolder.fileName = _strdup(videoNode->FirstChildElement("Filename")->GetText());
		if (videoNode->FirstChildElement("Link")) {
			vPlaceHolder.link = _strdup(videoNode->FirstChildElement("Link")->GetText());
			vPlaceHolder.linkExist = true;
		}
		else
			vPlaceHolder.linkExist = false;

		vPlaceHolder.fileExist = filesystem::exists(vPlaceHolder.fileName);
		if (videoNode->FirstChildElement("EndCumTime")) {
			vPlaceHolder.hasEndCum = true;
			vPlaceHolder.endCumTime = TimeTo_ms(videoNode->FirstChildElement("EndCumTime")->GetText());
		}
		else
			vPlaceHolder.hasEndCum = false;

		for (XMLNode* round = videoNode->FirstChildElement("Round"); round; round = round->NextSibling())
		{
			roundInfo rPlaceHolder;
			rPlaceHolder.startTime = TimeTo_ms(round->FirstChildElement("StartTime")->GetText());
			rPlaceHolder.endTime = TimeTo_ms(round->FirstChildElement("EndTime")->GetText());
			rPlaceHolder.skillRating = charToFloat(round->FirstChildElement("SkillRaiting")->GetText());
			if (round->FirstChildElement("CumMoment")) 
			{
				rPlaceHolder.hasCumMoment = true;
				for (XMLElement* moment = round->FirstChildElement("CumMoment"); moment; moment = moment->NextSiblingElement("CumMoment"))
					rPlaceHolder.cumMoment.push_back(TimeTo_ms(moment->GetText()));
			}
			else
				rPlaceHolder.hasCumMoment = false;

			vPlaceHolder.roundList.push_back(rPlaceHolder);
		}
		videoList.push_back(vPlaceHolder);
	}
	return videoList;
}

float charToFloat(const char* strFloat){
	float value = 0;

	value += strFloat[0] - '0';

	for (int i = 1; strFloat[i] != '\0'; i++) {
		if(strFloat[i] != '.')
		value += (strFloat[i] - '0') * pow(0.1f, i - 1);
	}
	return value;
}

libvlc_time_t TimeTo_ms(const char* strTime) {
	short int hh, mm, ss;
	for (int i = 0; strTime[i] != '\0'; i++) {
		if (strTime[i] == ':')
			continue;
		switch (i) 
		{
			case 0:
				hh = (strTime[i] - '0') * 10;
				break;
			case 1:
				hh += (strTime[i] - '0');
				break;
			case 3:
				mm = (strTime[i] - '0') * 10;
				break;
			case 4:
				mm += (strTime[i] - '0');
				break;
			case 6:
				ss = (strTime[i] - '0') * 10;
				break;
			case 7:
				ss += (strTime[i] - '0');
				break;
			default:
				throw new exception("Incorrect Time Format"); //<TODO> Proper error handling
		}
	}
	return ((hh * 60 + mm) * 60 + ss) * 1000;
}


﻿#include "stdafx.h"
#include "Resource.h"
#include "PlayListEditor.h"
#include <windows.h>
#include <ShObjIdl.h>
#include <shtypes.h>

// Forward declarations of functions included in this code module:
ATOM                RegisterPLEditorClass(HINSTANCE hInstance);
LRESULT CALLBACK    PLProc(HWND, UINT, WPARAM, LPARAM);
HRESULT LoadFile(UINT cFileTypes, __RPC__in_ecount_full(cFileTypes) const COMDLG_FILTERSPEC *rgFilterSpec);

//Global Variable
HWND plVidHWnd;
HINSTANCE plHInst;

HWND PELoadPLButton, PENewPLButton, PEAddVidButton, PEEditVidButton, PERoundinfoText;
HMENU PELoadPLMenu = (HMENU)IDM_PELoadPlaylist, PENewPLMenu = (HMENU)IDM_PENewPlayList, PEAddVidMenu = (HMENU)IDM_AddVideo, PEEditVidMenu = (HMENU)IDM_EditVideo, PERoundinfoMenu = (HMENU)IDM_PERoundInfo;
HWND PERoundPointButton, PEAddCumMomentButton, PEAddCumCountDownButton, PEUndoActionButton, PEDeleteButton;
HMENU PERoundPointMenu = (HMENU)IDM_RoundPoint, PEAddCumMomentMenu = (HMENU)IDM_AddCumMoment,  PEAddCumCountDownMenu = (HMENU)IDM_AddCumCountDown, PEUndoActionMenu = (HMENU)IDM_UndoPEAction, PEDeleteMenu = (HMENU)IDM_PEdelete;

const COMDLG_FILTERSPEC playListFileType[] =
{
	{ L"PlayList (*.xml)",       L"*.xml" }
};

void initPLEditor(HINSTANCE hInstance) {
	plHInst = hInstance;
	if (!RegisterPLEditorClass(hInstance))
		return;
	HWND hWnd = CreateWindowW(L"PlayListEditor", L"PlayList Editor", WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
		CW_USEDEFAULT, 0, 1280, 870, nullptr, nullptr, hInstance, nullptr);

	ShowWindow(hWnd, SW_SHOW);
}

ATOM RegisterPLEditorClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	wcex.lpfnWndProc = PLProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_HMVPLAYER));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"PlayListEditor";
	wcex.hIconSm = NULL;

	return RegisterClassExW(&wcex);
}


LRESULT CALLBACK PLProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//OutputDebugString(_T("" + message));
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		if (wmId >= 600 && wmId < 640)
		{
			SetFocus(hWnd);
			switch (wmId) {
			case IDM_PELoadPlaylist:
				LoadFile(_ARRAYSIZE(playListFileType), playListFileType);
				break;
			case IDM_PENewPlayList:

				break;
			case IDM_AddVideo:

				break;
			case IDM_EditVideo:

				break;
			case IDM_PERoundInfo:

				break;
			default:
				break;
			}
			break;
		}
		
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		FillRect(hdc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW));
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CREATE:
		CLIENTCREATESTRUCT ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 1);
		ccs.idFirstChild = 50001;
		plVidHWnd = CreateWindowEx(0, TEXT("MDICLIENT"), (LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN | CS_DBLCLKS, 0, 0, 1280, 720, hWnd, NULL, plHInst, (LPSTR)&ccs);
		ShowWindow(plVidHWnd, SW_SHOW);
		
		int x;
		PELoadPLButton = CreateWindow(L"BUTTON", L"Load PlayList", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x = 6, 746, 100, 25, hWnd, PELoadPLMenu, plHInst, NULL); x += 100;
		PENewPLButton = CreateWindow(L"BUTTON", L"New PlayList", WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x += 6, 746, 100, 25, hWnd, PENewPLMenu, plHInst, NULL); x += 100;
		PEAddVidButton = CreateWindow(L"BUTTON", L"Add Video", WS_VISIBLE | WS_CHILD | WS_DISABLED | BS_DEFPUSHBUTTON, x += 6, 746, 100, 25, hWnd, PEAddVidMenu, plHInst, NULL); x += 100;
		PEEditVidButton = CreateWindow(L"BUTTON", L"Edit Video", WS_VISIBLE | WS_CHILD | WS_DISABLED | BS_DEFPUSHBUTTON, x += 6, 746, 100, 25, hWnd, PEEditVidMenu, plHInst, NULL); x += 100;
		PERoundinfoText = CreateWindow(L"STATIC", L"RoundInfo", WS_VISIBLE | WS_CHILD | WS_BORDER | SS_CENTER |SS_CENTERIMAGE, x += 6, 746, 100, 25, hWnd, PERoundinfoMenu, plHInst, NULL); x += 100;

		PERoundPointButton = CreateWindow(L"BUTTON", L"Round Start", WS_VISIBLE | WS_CHILD | WS_DISABLED | BS_DEFPUSHBUTTON, x = 6, 781, 100, 25, hWnd, PERoundPointMenu, plHInst, NULL); x += 100;
		PEAddCumMomentButton = CreateWindow(L"BUTTON", L"Add CM", WS_VISIBLE | WS_CHILD | WS_DISABLED | BS_DEFPUSHBUTTON, x += 6, 781, 100, 25, hWnd, PEAddCumMomentMenu, plHInst, NULL); x += 100;
		PEAddCumCountDownButton = CreateWindow(L"BUTTON", L"Add CD", WS_VISIBLE | WS_CHILD | WS_DISABLED | BS_DEFPUSHBUTTON, x += 6, 781, 100, 25, hWnd, PEAddCumCountDownMenu, plHInst, NULL); x += 100;
		PEUndoActionButton = CreateWindow(L"BUTTON", L"Undo", WS_VISIBLE | WS_CHILD | WS_DISABLED | BS_DEFPUSHBUTTON, x += 6, 781, 100, 25, hWnd, PEUndoActionMenu, plHInst, NULL); x += 100;
		PEDeleteButton = CreateWindow(L"BUTTON", L"Delete", WS_VISIBLE | WS_CHILD | WS_DISABLED | BS_DEFPUSHBUTTON, x += 6, 781, 100, 25, hWnd, PEDeleteMenu, plHInst, NULL); x += 100;
		//Time Stamp

		break;
	case WM_SIZE:
		break;
	case WM_KEYDOWN:
		//<TODO> specific keys
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}



HRESULT LoadFile(
	/* [in] */ UINT cFileTypes,
	/* [size_is][in] */ __RPC__in_ecount_full(cFileTypes) const COMDLG_FILTERSPEC *rgFilterSpec)
{
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
		COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		IFileOpenDialog *pFileOpen;

		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));

		if (SUCCEEDED(hr))
		{
			// Set the options on the dialog.
			DWORD dwFlags;

			// Before setting, always get the options first in order 
			// not to override existing options.
			hr = pFileOpen->GetOptions(&dwFlags);
			if (SUCCEEDED(hr))
			{
				// In this case, get shell items only for file system items.
				hr = pFileOpen->SetOptions(dwFlags | FOS_FORCEFILESYSTEM);
				if (SUCCEEDED(hr))
				{
					// Set the file types to display only. 
					// Notice that this is a 1-based array.
					hr = pFileOpen->SetFileTypes(cFileTypes, rgFilterSpec);
					if (SUCCEEDED(hr))
					{
						// Set the selected file type index to Word Docs for this example.
						hr = pFileOpen->SetFileTypeIndex(1);
						if (SUCCEEDED(hr))
						{
							// Set the default extension to be ".doc" file.
							hr = pFileOpen->SetDefaultExtension(L"xml");
							if (SUCCEEDED(hr))
							{
								// Show the Open dialog box.
								hr = pFileOpen->Show(NULL);

								// Get the file name from the dialog box.
								if (SUCCEEDED(hr))
								{
									IShellItem *pItem;
									hr = pFileOpen->GetResult(&pItem);
									if (SUCCEEDED(hr))
									{
										PWSTR pszFilePath;
										hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

										// Display the file name to the user.
										if (SUCCEEDED(hr))
										{
											MessageBox(NULL, pszFilePath, L"File Path", MB_OK);
											CoTaskMemFree(pszFilePath);
										}
										pItem->Release();
									}
								}
								pFileOpen->Release();
							}
						}
					}
				}
			}
		}
		CoUninitialize();
	}
	return hr;
}
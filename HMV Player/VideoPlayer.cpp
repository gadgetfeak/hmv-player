#include "stdafx.h"
#include "VideoPlayer.h"

#include "vlcpp/vlc.hpp"
#include "VideoLoader.h"
#include <vector>
#include <thread>
using namespace std;

//global variable
VLC::MediaPlayer mp;
VLC::Instance instance = VLC::Instance(0, nullptr);
VLC::Media media;

vector<videoInfo> HMV_List;
libvlc_time_t startTime, endTime, endCumTime;
bool hasEndCum;
bool isInitialized = false;
int vidNum, rndNum;

int mode;
#define fhMode 0
#define hmvMode 1
FHConfig vfhConfig;
float averageSkill;
float usedSkill;
bool lastRound = false;
bool videoLoaded = false;

//Initializing
void videoCheck(HWND hwnd);
bool ifNotPlayed(int vidNum, int rndNum);
void fhPlayer(HWND);
void resetPlayer();

bool destroySelf = false;


void nextVideo(HWND hwnd) 
{
	if(isInitialized && !lastRound)
		switch (mode)
		{
		case fhMode:
			fhPlayer(hwnd);
			break;
		case hmvMode:
		default:
			mp.stop();
			break;
		}
}


bool playerControl(HWND hwnd, WPARAM wParam) {
	switch (wParam)
	{
	case IDM_MuteMENU:
		mp.toggleMute();
		break;
	case VK_SPACE:
		if (isInitialized)
			mp.pause();
		break;
	case VK_RIGHT:
		nextVideo(hwnd);
		break;
	case 'C':
		if (hasEndCum && isInitialized) {
			mp.setTime(endCumTime);
			rndNum = HMV_List[vidNum].roundList.size() - 1;
			endTime = HMV_List[vidNum].roundList[rndNum].endTime;
			if ((vfhConfig.skillBased)) {
				lastRound = true;
			}
		}
		break;
	case 'M':
		if (HMV_List[vidNum].roundList[rndNum].hasCumMoment && isInitialized)
		{
			bool found = false;
			int size = HMV_List[vidNum].roundList[rndNum].cumMoment.size();
			for (int i = 0; i < size; i++) {
				if (HMV_List[vidNum].roundList[rndNum].cumMoment[i] > mp.time())
				{
					mp.setTime(HMV_List[vidNum].roundList[rndNum].cumMoment[i]);
					found = true;
					break;
				}
			}
			if (!found)
				mp.setTime(HMV_List[vidNum].roundList[rndNum].cumMoment[size - 1]);
		}
		break;
	default:
		return false;
	}
	return true;
}

void destroyPlayer() {
	destroySelf = true;
}

void videoCheck(HWND hwnd) {
	while (true) {
		if (destroySelf)
			break;
		if(mp.isPlaying() || mp.time() >= mp.length()) //<TODO> Test time > lenght
			if (mp.time() >= endTime || mp.time() >= mp.length())
			{
				if (mode == fhMode && !vfhConfig.useVideoBreakTIme && !lastRound) {

					mp.setPause(true);
					int breaktime = (rand() % (vfhConfig.maxBreakTime - vfhConfig.minBreakTime + 1)) + vfhConfig.minBreakTime;
					this_thread::sleep_for(chrono::seconds(breaktime));
					mp.setPause(false);
				}
				nextVideo(hwnd);
				if (lastRound)
				{
					mp.stop();
					videoLoaded = false;
					resetPlayer();
				}
			}
		this_thread::sleep_for(chrono::seconds(1));
	}
}

bool isVideoLoaded() {
	return videoLoaded;
}

struct PlayedVid {
	int vidNum;
	int rndNum;
};

vector<PlayedVid> playedList;
int playedRepeated = 0;

bool ifNotPlayed(int vidNum, int rndNum) { //<TODO> improve by if the same video and round is repeated 3 times
	PlayedVid rndInf;
	rndInf.vidNum = vidNum;
	rndInf.rndNum = rndNum;
	if (playedList.size() > 0)
		for (int i = 0; i < playedList.size(); i++) 
			if (playedList[i].vidNum == rndInf.vidNum && playedList[i].rndNum == rndInf.rndNum)
			{
				if (playedRepeated > 3)
				{
					playedList.clear();
					break;
				}
				playedRepeated++;
				return false;
			}
	playedList.push_back(rndInf);
	playedRepeated = 0;
	return true;
}

#if _DEBUG
void TestFunction()
{
	LoadVideoList();
}
#endif

#pragma region Player Modes

int NotfoundCounter = 0;
void resetPlayer() {
	averageSkill = 0;
	usedSkill = 0;
	lastRound = false;
	NotfoundCounter = 0;
}

void setFhMode(HWND hwnd, FHConfig newFhConfig) {

	resetPlayer();

	vfhConfig = newFhConfig;
	mode = fhMode;
	fhPlayer(hwnd);
}


void fhPlayer(HWND hwnd) {
	if (HMV_List.size() <= 0) {
		HMV_List = LoadVideoList();
		if (HMV_List.size() <= 0) {
			MessageBox(NULL, L"No video in playlist", NULL, MB_OK);
		}
		bool isEmpty = true;
		for (int i = 0; i < HMV_List.size(); i++) {
			if (HMV_List[i].fileExist || HMV_List[i].linkExist) {
				isEmpty = false;
				break;
			}
		}
		if (isEmpty) {
			MessageBox(NULL, L"No matching video found", NULL, MB_OK);
			HMV_List.clear();
			return;
		}
	}
	if (vfhConfig.skillBased && averageSkill == 0) {
		int count = 0;
		for(int i = 0; i < HMV_List.size(); i++){
				averageSkill += HMV_List[i].roundList[HMV_List[i].roundList.size() - 1].skillRating;
				count++;
			}
		averageSkill /= count;
	}

	srand(time(NULL));
	while (true)
	{
		vidNum = rand() % HMV_List.size();
		if (HMV_List[vidNum].fileExist || HMV_List[vidNum].linkExist)
		{
			if(HMV_List[vidNum].fileExist)
				media = VLC::Media(instance, HMV_List[vidNum].fileName, VLC::Media::FromPath);
			else
				media = VLC::Media(instance, HMV_List[vidNum].link, VLC::Media::FromLocation); //<TODO> Handling not loading
			rndNum = rand() % HMV_List[vidNum].roundList.size();  //<TODO> Error handling
			if (vfhConfig.simplyRandom && !vfhConfig.includeFinaleRound) {
				if (rndNum == HMV_List[vidNum].roundList.size() - 1)
					continue;
			}
			else if (vfhConfig.skillBased) {
				if ((vfhConfig.skillLevel - usedSkill <= averageSkill) || NotfoundCounter >= 10)
				{
					if (rndNum != HMV_List[vidNum].roundList.size() - 1)
						continue;
					else
						lastRound = true;
				}
				else if (rndNum == HMV_List[vidNum].roundList.size() - 1) {
					if (HMV_List[vidNum].roundList[rndNum].skillRating >= vfhConfig.skillLevel - usedSkill)
						lastRound = true;
					else
						continue;
				}
				else if (HMV_List[vidNum].roundList[rndNum].skillRating >= vfhConfig.skillLevel - usedSkill) {
					if(NotfoundCounter++ < 10)
						continue;
				}
			}

				if (ifNotPlayed(vidNum, rndNum)) {
					startTime = HMV_List[vidNum].roundList[rndNum].startTime;
					if (vfhConfig.useVideoBreakTIme) {
						if (rndNum >= HMV_List[vidNum].roundList.size() - 1)
							endTime = mp.length() - 1;
						else
							endTime = HMV_List[vidNum].roundList[rndNum + 1].startTime;
					}
					else
						endTime = HMV_List[vidNum].roundList[rndNum].endTime;
					hasEndCum = HMV_List[vidNum].hasEndCum;
					if (hasEndCum)
						endCumTime = HMV_List[vidNum].endCumTime;

					NotfoundCounter = 0;
					usedSkill += HMV_List[vidNum].roundList[rndNum].skillRating;
					break;
				}
		}
	}
	
	mp = VLC::MediaPlayer(media); //<TODO> Improve re-initialization
	mp.setHwnd(hwnd);
	//mp.setFullscreen(true);
	libvlc_time_t mediaTime = mp.time();
	mp.play();
	mp.setTime(startTime);
	if (!isInitialized) {
		thread(videoCheck, hwnd).detach();
		isInitialized = true;
	}
	videoLoaded = true;
	//std::this_thread::sleep_for(std::chrono::seconds(10));
	//mp.stop();
}

#pragma endregion
#pragma once
#include "stdafx.h"

struct FHConfig
{
	bool simplyRandom;
	bool includeFinaleRound;
	bool skillBased;
	int skillLevel;
	bool useVideoBreakTIme;
	int minBreakTime;
	int maxBreakTime;
};

//void player(HWND hwnd);
bool playerControl(HWND, WPARAM);
void destroyPlayer();
void nextVideo(HWND);
void setFhMode(HWND, FHConfig);
bool isVideoLoaded();
#if _DEBUG
void TestFunction();
#endif
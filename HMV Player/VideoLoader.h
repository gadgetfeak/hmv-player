#pragma once
#ifndef videoLoaderStructure
#define videoLoaderStructure
#include <vector>

struct roundInfo {
	libvlc_time_t startTime;
	libvlc_time_t endTime;
	float skillRating;
	bool hasCumMoment;
	std::vector<libvlc_time_t> cumMoment;
	//<TODO> Tag information
	//<TODO> Nearest Cum Momemt if multiple. record all
};

struct videoInfo {
	char* fileName;
	char* videoName; //<TODO>
	char* link;
	bool fileExist;
	bool linkExist;
	libvlc_time_t videoLength; //<TODO>
	std::vector<roundInfo> roundList;
	//<TODO>add verification
	bool hasEndCum; //<TODO> Nearest Cum Momemt if multiple. record all
	libvlc_time_t endCumTime;
};
#endif


std::vector<videoInfo> LoadVideoList();